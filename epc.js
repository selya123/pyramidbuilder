class PyramidBuild {
    constructor(layers) {
        this.layers = layers
        this.number_of_bricks = this.getBricks()
        this.bricks_remaining = 0
        this.render_start_button()
    }

    *
    getBricks() {
        let i = this.layers;
        while (i > 0) {
            yield i--;
        }
    }

    addBricks(z) {
        let row_z = document.getElementById("r" + z);
        for (let i = 1; i < this.bricks_remaining; i++) {
            let brick = document.createElement("div");
            brick.innerHTML = "b" + i;
            brick.setAttribute("class", "brick");
            brick.setAttribute("id", "b" + i);
            row_z.append(brick);
        }
    }

    render_start_button() {
        let _start_button = document.getElementById('startbutton')
        _start_button.addEventListener('click', e => {
            let des = document.getElementById("description");
            des.innerHTML = "";
            e.stopPropagation();
            let build_btn = document.createElement('span')
            build_btn.setAttribute('id', 'build-btn')
            build_btn.innerText = "Build!"
            document.getElementById('score').append(build_btn)
            this.click_build(build_btn);
        })
    }

    click_build(build_button) {
        let _brick_remaining = document.querySelector(".bricks-remaining");
        build_button.addEventListener(
            "click",
            (e) => {
                let row = document.createElement("div");
                row.setAttribute("class", "row");
                this.bricks_remaining = this.number_of_bricks.next().value;
                _brick_remaining.innerHTML = this.bricks_remaining > 2 ? (this.bricks_remaining * (this.bricks_remaining - 1)) / 2 - 1 : "0";
                let z = -(1 - this.bricks_remaining);
                if (z > 0) row.setAttribute("id", "r" + z);
                block.prepend(row);
                this.addBricks(z);
                e.stopPropagation();
            }
        );
    }
}

const builder = new PyramidBuild(15);